package cz.fi.muni.pa165.tasks;

import cz.fi.muni.pa165.PersistenceSampleApplicationContext;
import cz.fi.muni.pa165.entity.Category;
import cz.fi.muni.pa165.entity.Product;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.validation.ConstraintViolationException;
import java.util.Set;

 
@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
public class Task02 extends AbstractTestNGSpringContextTests {

	@PersistenceUnit
	private EntityManagerFactory emf;

	Category kitchen = new Category();
	Category electro = new Category();
	Product flashlight = new Product();
	Product KitchenRobot = new Product();
	Product Plate = new Product();

	private void assertContainsCategoryWithName(Set<Category> categories, String expectedCategoryName) {
		for(Category cat: categories){
			if (cat.getName().equals(expectedCategoryName))
				return;
		}
			
		Assert.fail("Couldn't find category " + expectedCategoryName + " in collection " + categories);
	}
	private void assertContainsProductWithName(Set<Product> products, String expectedProductName) {
		for(Product prod: products){
			if (prod.getName().equals(expectedProductName))
				return;
		}
			
		Assert.fail("Couldn't find product " + expectedProductName + " in collection " + products);
	}

	@BeforeClass
	private void initialize(){

		kitchen.setName("Kitchen");
		electro.setName("Electro");
		flashlight.setName("Flashlight");
		KitchenRobot.setName("Kitchenrobot");
		Plate.setName("Plate");
		kitchen.addProduct(KitchenRobot);
		electro.addProduct(flashlight);
		kitchen.addProduct(Plate);
		Plate.addCategory(kitchen);
		flashlight.addCategory(electro);
		KitchenRobot.addCategory(kitchen);
		KitchenRobot.addCategory(electro);

		var em = emf.createEntityManager();

		em.getTransaction().begin();
		em.persist(electro);
		em.persist(kitchen);
		em.persist(flashlight);
		em.persist(KitchenRobot);
		em.persist(Plate);

		em.getTransaction().commit();

	}

	@Test
	public void test01(){
		var em = emf.createEntityManager();
		var expected_electro = em.find(Category.class,electro.getId());
		assertContainsProductWithName(expected_electro.getProducts(),flashlight.getName());
		assertContainsProductWithName(expected_electro.getProducts(),KitchenRobot.getName());
	}
	@Test
	public void test02(){
		var em = emf.createEntityManager();
		var expected_kitchen = em.find(Category.class,kitchen.getId());
		assertContainsProductWithName(expected_kitchen.getProducts(),Plate.getName());
		assertContainsProductWithName(expected_kitchen.getProducts(),KitchenRobot.getName());
	}
	@Test
	public void test03(){
		var em = emf.createEntityManager();
		var expected_robot = em.find(Product.class,KitchenRobot.getId());
		assertContainsCategoryWithName(expected_robot.getCategories(),kitchen.getName());
		assertContainsCategoryWithName(expected_robot.getCategories(),electro.getName());
	}
	@Test
	public void test04(){
		var em = emf.createEntityManager();
		var expected = em.find(Product.class,flashlight.getId());
		assertContainsCategoryWithName(expected.getCategories(),electro.getName());
	}
	@Test
	public void test05(){
		var em = emf.createEntityManager();
		var expected = em.find(Product.class,Plate.getId());
		assertContainsCategoryWithName(expected.getCategories(),kitchen.getName());
	}

	@Test(expectedExceptions= ConstraintViolationException.class)
	public void testDoesntSaveNullName() {
		var em = emf.createEntityManager();
		Product prod = new Product();
		em.getTransaction().begin();
		em.persist(prod);
		em.getTransaction().commit();
	}
	}


