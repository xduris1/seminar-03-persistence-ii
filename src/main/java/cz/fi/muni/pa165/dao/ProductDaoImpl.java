package cz.fi.muni.pa165.dao;

import cz.fi.muni.pa165.entity.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao{
    @PersistenceContext
    EntityManager em;


    @Override
    public void create(Product p) {
        em.persist(p);
    }
    @Override
    public List<Product> findAll() {
        return em.createQuery("select p from Product p", Product.class).getResultList();
    }
    @Override
    public Product findById(Long id) {
        return em.find(Product.class,id);
//        return em.createQuery("select p from Product p where p.id = id", Product.class).getSingleResult();
    }
    @Override
    public void remove(Product p) {
        em.remove(p);
    }
    @Override
    public List<Product> findByName(String name) {
        return em.createQuery("select p from Product p where name = :param", Product.class).setParameter("param",name).getResultList();
    }
}
